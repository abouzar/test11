FROM python:3.7

ADD requirements.txt .

RUN pip install -r requirements.txt  --use-feature=2020-resolver

EXPOSE 8080

ADD . .

RUN ["chmod", "+x", "-R", "./scripts/"]

CMD ./scripts/upstart.sh


