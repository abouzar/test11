#!/bin/sh

python3 manage.py migrate --noinput
if [ $? -ne 0 ]; then
    echo "Migration failed." >&2
    exit 1
fi

python3 manage.py runserver 0.0.0.0:8080
