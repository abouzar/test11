#!/usr/bin/env sh

echo "Checking migrations..."
python3 manage.py makemigrations --no-input --dry-run --check


echo "Running tests..."
python3 manage.py test --no-input
