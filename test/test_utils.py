from unittest import TestCase

from utils.persian import convert_to_persian_numbers


class Test(TestCase):
    def test_convert_to_persian_numbers(self):
        text = "1234567890"
        expected = "۱۲۳۴۵۶۷۸۹۰"
        got = convert_to_persian_numbers(text)
        self.assertEqual(got, expected, "")
